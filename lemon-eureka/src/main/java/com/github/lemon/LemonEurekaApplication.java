package com.github.lemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * eurekaServer服务启动类，接受其它微服务注册进来
 * @author lpf
 */
@SpringBootApplication
@EnableEurekaServer
public class LemonEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(LemonEurekaApplication.class, args);
    }
}
