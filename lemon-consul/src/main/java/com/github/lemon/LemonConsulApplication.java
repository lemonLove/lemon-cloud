package com.github.lemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * eurekaServer服务启动类，接受其它微服务注册进来
 * @author lpf
 */
@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class LemonConsulApplication {

    @RequestMapping("/")
    public String home() {
        return "Hello world";
    }

    public static void main(String[] args) {
        SpringApplication.run(LemonConsulApplication.class, args);
    }
}
