package com.github.lemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LemonConfgClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(LemonConfgClientApplication.class, args);
    }
}
