package com.github.lemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.github.lemon"})
@ComponentScan("com.github.lemon")
public class LemonConsumerFeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(LemonConsumerFeignApplication.class, args);
	}
}
