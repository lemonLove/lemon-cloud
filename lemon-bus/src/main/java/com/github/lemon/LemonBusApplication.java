package com.github.lemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LemonBusApplication {

    public static void main(String[] args) {
        SpringApplication.run(LemonBusApplication.class, args);
    }
}
