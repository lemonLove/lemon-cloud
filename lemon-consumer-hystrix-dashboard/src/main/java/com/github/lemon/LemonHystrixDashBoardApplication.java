package com.github.lemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableHystrixDashboard
public class LemonHystrixDashBoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(LemonHystrixDashBoardApplication.class, args);
	}
}
