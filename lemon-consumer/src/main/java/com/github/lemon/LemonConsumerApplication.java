package com.github.lemon;

import com.github.myrule.MySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name="LEMON-DEPT",configuration = MySelfRule.class)
public class LemonConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LemonConsumerApplication.class, args);
	}
}
